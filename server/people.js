Meteor.publish('people.id', function(id) {
  return CRM.Collections.People.find(id ? {
         _id: id
     } : {});
});

Meteor.publish('people.all', function() {
    return CRM.Collections.People.find({});
});

Meteor.publish('people.archived', function() {
    return CRM.Collections.People.find({
        removed: true
    });
});

Meteor.methods({
    'people.create': create,
    'people.update': update,
    'people.all':all,
    'people.name':name,
    'people.tip':tip,
    'people.clean':clean
});

function all() {
    return CRM.Collections.People.find({});
}

function create(people) {
    var result = CRM.Collections.People.insert({people:people});
    return result
}

function update(id,array,categorie) {
  if(categorie == "films"){
    CRM.Collections.People.update({
        _id: id
    }, {
        $set: {"films":array}
    }, {});
  }
}

function name(id,name,score) {
    CRM.Collections.People.update({
        _id: id
    }, {
        $set: {"nameSelected":name,"score":score}
    }, {});
}

function clean() {
     CRM.Collections.People.find({}).forEach(function(people) {

        CRM.Collections.People.update({
            _id: people._id
        }, {
            $unset: {"nameSelected":"","score":0,"tip":false}
        }, {});
     });;

}

function tip(id,status) {

    CRM.Collections.People.update({
        _id: id
    }, {
        $set: {"tip":status}
    }, {});
}
