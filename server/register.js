Meteor.publish('register.id', function(id) {
  return CRM.Collections.Register.find(id ? {
         _id: id
     } : {});
});

Meteor.publish('register.all', function() {
    return CRM.Collections.Register.find({});
});

Meteor.publish('register.archived', function() {
    return CRM.Collections.Register.find({
        removed: true
    });
});

Meteor.methods({
    'register.create': create,
    'register.all':all
});

function all() {
    return CRM.Collections.Register.find({});
}

function create(register) {
    var result = CRM.Collections.Register.insert(register);
    return result
}
