function requestURL(params){
  var url = params;

			var result = Meteor.http.get(url, {timeout:30000});
			if(result.statusCode==200) {
				var respJson = JSON.parse(result.content);
				return respJson;
			} else {
				console.log("Response issue: ", result.statusCode);
				var errorJson = JSON.parse(result.content);
				throw new Meteor.Error(result.statusCode, errorJson.error);
			}
}

Meteor.startup(function () {

	if (CRM.Collections.People.find().count() < 1) {

    var result = requestURL('https://swapi.co/api/people');

      if(result != undefined){
          console.log("start");
          for(var i=0; i < result.results.length; i++){
              Meteor.call('people.create',result.results[i]);
          }
          reload();
      }
	}

});

function reload(){
    return new Promise (function (resolve, reject) {
      CRM.Collections.People.find({}).fetch().forEach(function(people) {
        var array = [];
        if(people.people.films && people.people.films.length> 0){
          for(var i =0; i < people.people.films.length; i++){
            doRequestFilms(people.people.films[i],people._id);
          }
        }

        if(people.people.species && people.people.species.length> 0){
          for(var i =0; i < people.people.species.length; i++){
            doRequestSpecies(people.people.species[i],people._id);
          }

        }

        if(people.people.vehicles && people.people.vehicles.length> 0){
          for(var i =0; i < people.people.vehicles.length; i++){
            doRequestVehicles(people.people.vehicles[i],people._id);
          }
        }

        if(people.people.homeworld ){
            doRequestHomeworld(people.people.homeworld,people._id);
        }

        if(people.people.name){
            doRequestImage(people.people.name,people._id);
        }

      });

      });
}

function doRequestImage(params,id) {
  var url = 'https://www.googleapis.com/customsearch/v1?q='+params+'&cx=005267924509827333915:ufejhcp94t4&searchType=image&key=AIzaSyDIaPZ04cmwgKPxKMdGeIZuWOz0yyjIoRU';
  return new Promise (function (resolve, reject) {
    Meteor.http.get(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {

        var respJson = JSON.parse(res.content);

        if(respJson && respJson.items && respJson.items.length > 0){
              CRM.Collections.People.update({
                  _id: id
              }, {
                  $set: {
                      "image":respJson.items[0].link
                  }
              });
        }

      } else {
        console.log("error")
      }
    });
  });
}

function doRequestHomeworld(url,id) {
  return new Promise (function (resolve, reject) {
    Meteor.http.get(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        var respJson = JSON.parse(res.content);
            CRM.Collections.People.update({
                _id: id
            }, {
                $set: {
                    "homeworld":respJson.name
                }
            });
      } else {
        console.log("error")
      }
    });
  });
}


function doRequestFilms(url,id) {
  return new Promise (function (resolve, reject) {
    Meteor.http.get(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        var respJson = JSON.parse(res.content);
            var people = CRM.Collections.People.findOne({_id:id});
            var films = [];
            if(people.films && people.films.length > 0){
              films = people.films;
            }
            films.push(respJson.title);
            CRM.Collections.People.update({
                _id: id
            }, {
                $set: {
                    "films":films
                }
            });
      } else {
        console.log("error")
      }
    });
  });
}

function doRequestVehicles(url,id) {
  return new Promise (function (resolve, reject) {
    Meteor.http.get(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        var respJson = JSON.parse(res.content);
            var people = CRM.Collections.People.findOne({_id:id});
            var vehicles = [];
            if(people.vehicles && people.vehicles.length > 0){
              vehicles = people.vehicles;
            }
            vehicles.push(respJson.name);
            CRM.Collections.People.update({
                _id: id
            }, {
                $set: {
                    "vehicles":vehicles
                }
            });
      } else {
        console.log("error")
      }
    });
  });
}

function doRequestSpecies(url,id) {
  return new Promise (function (resolve, reject) {
    Meteor.http.get(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        var respJson = JSON.parse(res.content);
            var people = CRM.Collections.People.findOne({_id:id});
            var species = [];
            if(people.species && people.species.length > 0){
              species = people.species;
            }
            species.push(respJson.name);
            CRM.Collections.People.update({
                _id: id
            }, {
                $set: {
                    "species":species
                }
            });
      } else {
        console.log("error")
      }
    });
  });
}
