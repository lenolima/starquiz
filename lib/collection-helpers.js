createCollection = function (collectionName, schema) {

	var GenericCollection = _.extend(Mongo.Collection, {});

	var definedCollection = new GenericCollection(collectionName);

	// TODO: Deny all client-side updates since we will be using methods to manage this collection
	// TODO: Set indexes (not here but in every collection declaration)
	definedCollection.allow({
		insert: function (userId, user, fields, modifier) {
			return true;
		},
		update: function (userId, user, fields, modifier) {
			return true;
		},
		remove: function (userId, user, fields, modifier) {
			return true;
		},
	});

	definedCollection.attachBehaviour('softRemovable');

	definedCollection.attachSchema(schema);

	return definedCollection;

}
