Star Quiz

When using Mac or Linux you should use the included deploy script
to run this application even when running locally.

`./deploy --env=local`
or
`meteor --settings mup/development/settings.json --port 3000`


OSX / LINUX
Run the following command in your terminal to install the latest official Meteor release:
`curl https://install.meteor.com/ | sh`
