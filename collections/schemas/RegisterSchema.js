CRM.Schemas.RegisterSchema = (function() {
    return new SimpleSchema({
       name: {
         type: String,
         optional:true
       },
       email: {
           type: String,
           optional:true
       },
       score: {
           type: Number,
           optional:true
       },
       time: {
           type: String,
           optional:true
       }
    });
})();
