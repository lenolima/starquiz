CRM.Schemas.PeopleSchema = (function() {
    return new SimpleSchema({
       people: {
           type: Object,
           blackbox: true,
           optional:true
       },
       films: {
           type: [String],
           optional:true
       },
       homeworld: {
           type: String,
           optional:true
       },
       vehicles: {
           type: [String],
           optional:true
       },
       species: {
           type: [String],
           optional:true
       },
       homeworld: {
           type: String,
           optional:true
       },
       image: {
           type: String,
           optional:true
       },
       nameSelected: {
           type: String,
           optional:true
       },
       tip: {
           type: Boolean,
           optional:true
       },
       score: {
           type: Number,
           optional:true
       }
    });
})();
