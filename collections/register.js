
/**
 * Register
 * @type {Mongo.Collection}
 */
CRM.Collections.Register =  new Mongo.Collection("Register");

CRM.Collections.Register.attachBehaviour("softRemovable");

CRM.Collections.Register.allow({
    insert: function(registerId, register, fields, modifier)
    {
        // TODO: security
        return true;
    },
    update: function(registerId, register, fields, modifier)
    {
        // TODO: security
        return true;
    },
    remove: function(registerId, register, fields, modifier)
    {
        // TODO: security
        return true;
    },
});

Meteor.startup(function(){
    CRM.Collections.Register.attachSchema(CRM.Schemas.RegisterSchema);
});
