
/**
 * People
 * @type {Mongo.Collection}
 */
CRM.Collections.People =  new Mongo.Collection("People");

CRM.Collections.People.attachBehaviour("softRemovable");

CRM.Collections.People.allow({
    insert: function(peopleId, people, fields, modifier)
    {
        // TODO: security
        return true;
    },
    update: function(peopleId, people, fields, modifier)
    {
        // TODO: security
        return true;
    },
    remove: function(peopleId, people, fields, modifier)
    {
        // TODO: security
        return true;
    },
});

Meteor.startup(function(){
    CRM.Collections.People.attachSchema(CRM.Schemas.PeopleSchema);
});
