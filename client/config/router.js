Router.configure({
    layoutTemplate: 'mainLayout',
    notFoundTemplate: 'notFound'

});

//Game
Router.route('/home', function () {
    this.render('home');
    this.layout('blankLayout')
});

Router.route('/quiz', function () {
    this.render('quiz');
    this.layout('blankLayout')
});


Router.route('/notFound', function () {
    if (!(Meteor.user() || Meteor.loggingIn()))
        Router.go('home');
    else
        this.render('notFound');
});

// Default route
// You can use direct this.render('template')

Router.route('/', function () {
    Router.go('home');
});

var mustBeSignedIn = function (pause) {
  if (!(Meteor.user() || Meteor.loggingIn())) {
    Router.go('home');
  } else {
    this.next();
  }
};

var goToDashboard = function(pause) {
  if (Meteor.user()) {
    Router.go('home');
  } else {
    this.next();
  }
};


Router.onBeforeAction(mustBeSignedIn, {except: ['home','quiz']});
Router.onBeforeAction(goToDashboard, {only: ['index']});
