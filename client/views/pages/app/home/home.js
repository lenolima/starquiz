

Template.home.onCreated(function() {
    var _this = this;
    _this.autorun(function() {
      _this.subscribe('people.all');
      _this.subscribe('register.all');
    })

    var array = CRM.Collections.People.find({});
});

Template.home.onRendered(function() {
    var _this = this;
    _this.autorun(function() {

      if (_this.subscriptionsReady()) {
        _this.subscribe('people.all');
        _this.subscribe('register.all');
      }

      if(Session.get("ShowRanking")){
        $("#show-ranking-form").modal("show");
      }

    })
});

Template.home.events({
    'click #play': function(event){
        event.preventDefault();
        Router.go("quiz");
    },
    'click #closeRanking': function(event){
      console.log("close");
      $("#show-ranking-form").modal("hide");
      Session.set("ShowRanking",false);
    },


});
Template.home.helpers({
register: function(){
  var options = {
      'skip': 0,
      'limit': 10,
      "sort" : {"score": -1}
    };
    return CRM.Collections.Register.find({},options);
},
position: function(index){
  if(index){
    return parseInt(index)+1;
  }else{
    return "<i class='fa fa-trophy' aria-hidden='true'></i>";
  }
}
})
