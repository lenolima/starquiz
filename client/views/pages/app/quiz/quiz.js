var currentTab = 0;
var countTab = 0;
var minutes = 0;
var seconds = 0;
var countInteval = null;

Template.quiz.onCreated(function() {
  var _this = this;

  _this.selectedDetail = new ReactiveVar(null);
  _this.selectDefaultValue = new ReactiveVar(null);
  _this.scoreFinal = new ReactiveVar(0);
  _this.totalPeople = new ReactiveVar(0);
  _this.tabs = new ReactiveVar(null);
  _this.peopleNames = new ReactiveVar(null);
  _this.timerCount = new ReactiveVar('02:00');

  _this.autorun(function() {
    _this.subscribe('people.all');
    _this.subscribe('register.all');
    currentTab = 0;
    countTab = 0;
  })
});

Template.quiz.onRendered(function() {
  var _this = this;

  Session.set("ShowRanking", false);

  _this.autorun(function() {
    //_this.subscribe('people.all');
    //_this.subscribe('register.all');
    _this.totalPeople.set(CRM.Collections.People.find({}).count());

    if(_this.totalPeople.get() > 0){
      var numberTabs = Math.round(_this.totalPeople.get()/4);
      var tabs = [];
      for(var i =0; i< numberTabs;i++){
        tabs.push({"skip":(i*4),'limit': 4});
      }
      console.log("tabs:",tabs);
      _this.tabs.set(tabs);
    }

    var names = [];
    CRM.Collections.People.find({},{"sort": {"people.created": -1}}).fetch().forEach(function(people) {
      names.push({"name":people.people.name});
    });
    _this.peopleNames.set(names);

    if (_this.subscriptionsReady()) {
        if(!_this.selectDefaultValue.get() && $("#selectPersonName").val() == "Selecione um nome"){
            _this.selectDefaultValue.set($("#selectPersonName").val());
        }
    }

    var modalShowTab = setInterval(function() {
        clearInterval(modalShowTab);
        showTab(currentTab);
        console.log("showTab");
    },500);
    
  });

  Meteor.call('people.clean');

  $('.modal').appendTo("body");

  $('#show-roles-form').modal({
    backdrop: 'static',
    keyboard: true,
    show: true
  });

});

Template.quiz.helpers({
  timerCount: function(){
    return Template.instance().timerCount.get();
  },
  creatTabs: function() {
    var numberTabs = Math.round(Template.instance().totalPeople.get()/4);
    if(Template.instance().tabs.get() && Template.instance().tabs.get().length == numberTabs){
      return Template.instance().tabs.get();
    }
  },
  register: function() {
    var options = {
      'skip': 0,
      'limit': 10,
      "sort": {
        "score": -1
      }
    };
    return CRM.Collections.Register.find({}, options);
  },
  scoreFinal: function() {
    return Template.instance().scoreFinal.get();
  },
  position: function(index) {
    if (index) {
      return parseInt(index) + 1;
    } else {
      return "<i class='fa fa-trophy' aria-hidden='true'></i>";
    }
  },
  people: function(skip,limit) {
    var options = {
      'skip': skip,
      'limit': limit
    };

    console.log("options:",options);
    return CRM.Collections.People.find({}, options);
  },
  peopleNames: function() {
    if(Template.instance().peopleNames.get()){
      return Template.instance().peopleNames.get();
    }
  },
  detail: function() {
    if (Template.instance().selectedDetail.get()) {
      return Template.instance().selectedDetail.get();
    }
  },
  detailSessionArray: function(array) {
    if (array != undefined) {
      return arrayTostring(array);
    }
  },
  showVehicles: function(array) {
    if (array != undefined) {
      return true;
    }
    return false;
  }
});

Template.quiz.events({
'click #startQuiz': function(event, templateInstance) {
  $("#show-roles-form").modal("hide");
  startCountDown(templateInstance);
},
'click .detail': function(event, templateInstance) {
  $("#show-detail-form").modal("show");
  var people = CRM.Collections.People.findOne({
    _id: event.target.id
  });
  templateInstance.selectedDetail.set(people);
  Meteor.call("people.tip", event.target.id, true);
},
'click .personName': function(event, templateInstance) {
  $("#show-input-form").modal("show");
  $("#selectPersonName").val(templateInstance.selectDefaultValue.get());
  $('#selectPersonName').attr("disabled", false);
  var people = CRM.Collections.People.findOne({
    _id: event.target.id
  });

  if (people.nameSelected != undefined) {
    $("#selectPersonName").val(people.nameSelected);
    $('#selectPersonName').attr("disabled", true);
  }
  templateInstance.selectedDetail.set(people);
},
'click #savePersonName': function(event, templateInstance) {

  var people = CRM.Collections.People.findOne({
    _id: event.target.name
  });

  if ($("#selectPersonName").val() == templateInstance.selectDefaultValue.get() && people.nameSelected == undefined) {

    sweetAlert({
      title: "StarQuiz",
      text: "Você precisa selecionar um nome!",
      type: "warning",
      showCancelButton: false,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "ok",
      closeOnConfirm: true
    }, function() {

    });

  } else {


    if (people) {
      $("#show-input-form").modal("hide");
      var score = 0;
      if (people.people.name == $("#selectPersonName").val()) {

        if (people.tip == undefined) {
          score = 10;
        } else {
          score = 5;
        }
      }

      Meteor.call("people.name", people._id, $("#selectPersonName").val(), score, function(err, result) {

        var peopleCount = templateInstance.totalPeople.get();
        var peoplePunctuated = CRM.Collections.People.find({
          score: {
            "$in": [0, 5, 10]
          }
        });

        if (peoplePunctuated.count() == peopleCount) {
          clearInterval(countInteval);
          var scoreFinal = 0;
          peoplePunctuated.forEach(function(people) {
            scoreFinal += people.score;
          });

          templateInstance.scoreFinal.set(scoreFinal);

          var modalOpen = setInterval(function() {
              clearInterval(modalOpen);
              $("#show-finish-form").modal("show");
          }, 500);

        }

      });

    }
  }
},
'click #saveUser': function(event) {

  var name = $("#userName").val();
  var email = $("#email").val();
  var timer = '0' + minutes + ':' + seconds;

   if (name == "" || email == "" || !validateEmail(email)) {

    sweetAlert({
      title: "StarQuiz",
      text: "insira seu nome e email um e-mail válido!",
      type: "warning",
      showCancelButton: false,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "ok",
      closeOnConfirm: true
    }, function() {

    });

  } else {
    var register = {
      email: email,
      name: name,
      score: Template.instance().scoreFinal.get(),
      time: timer
    }

    Session.set("ShowRanking", true);
    Meteor.call('register.create', register);
    $("#show-finish-form").modal("hide");

    sweetAlert({
      title: "StarQuiz",
      text: "Obrigado por participar do StarQuiz !",
      type: "success",
      showCancelButton: false,
      confirmButtonColor: "#AEDEF4",
      confirmButtonText: "ok",
      closeOnConfirm: true
    }, function() {


      Router.go('home');

    });
  }
},
'click #closeRanking': function(event) {
  $("#show-ranking-form").modal("hide");
},
'click #prevBtn': function(event) {

  if (countTab > 0) {
    countTab--;
    nextPrev(countTab);
  }

},
'click #nextBtn': function(event) {
  var x = document.getElementsByClassName("tab");
  if (countTab < x.length - 1) {
    countTab++
    nextPrev(countTab);
  }
}

})

function showTab(n) {

  var x = document.getElementsByClassName("tab");

  x[n].style.display = "block";

  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }

  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").style.display = "none";
  } else {
    document.getElementById("nextBtn").style.display = "inline";
  }

  fixStepIndicator(n)
}

function nextPrev(n) {

  var x = document.getElementsByClassName("tab");

  x[currentTab].style.display = "none";
  currentTab = n;

  if (currentTab >= x.length) {
    return false;
  }

  showTab(currentTab);
}

function fixStepIndicator(n) {
  var i, x = document.getElementsByClassName("step-tab");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  x[n].className += " active";
}

function arrayTostring(array) {
  var names = "";
  for (var i = 0; i < array.length; i++) {
    if (i < array.length - 1) {
      names += array[i] + " ,"
    } else {
      names += array[i]
    }
  }

  return names;
}

function startCountDown(templateInstance) {

   var countDownDate = moment().add(120, 'seconds').toDate();
   var count = 0;

   countInteval = setInterval(function() {
      var now = new Date().getTime();

      var distance = countDownDate - now;

      minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      seconds = Math.floor((distance % (1000 * 60)) / 1000);

      if (seconds < 10) {
        seconds = '0' + seconds;
      }

      templateInstance.timerCount.set('0' + minutes + ':' + seconds);

      if (distance < 0) {
          clearInterval(countInteval);

          templateInstance.timerCount.set('00:00');

          var peoplePunctuated = CRM.Collections.People.find({
            score: {
              "$in": [0, 5, 10]
            }
          });

          var scoreFinal = 0;

          peoplePunctuated.forEach(function(people) {
            scoreFinal += people.score;
          });

          templateInstance.scoreFinal.set(scoreFinal);
          $("#show-finish-form").modal("show");

      }
  }, 500);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
