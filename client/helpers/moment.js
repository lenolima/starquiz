Template.registerHelper('momentFromNow', function (date) {
	return moment(date).fromNow();
});

Template.registerHelper('momentFormat', function (date) {
	var df = "";
	if(date == null){
		df = "No install date";
	}else{
		df = moment(date).format('dddd MM/DD/YYYY, h:mm a');
	}
	return df;
});
