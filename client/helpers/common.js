Template.registerHelper('emptyOBJ', function () {
	return {};
});

Template.registerHelper('userIsInRole', function (role) {
	return Roles.userIsInRole(Meteor.user(), role)
});

Template.registerHelper('getUserNameByID', function (userId) {
	var user = CRM.Collections.Users.findOne(userId);
	return user.profile.firstName + ' ' + user.profile.lastName;
});

Template.registerHelper('statusLabel', function (status) {
	var statusLabel = '';
	switch(status) {
	case 'Active':
			statusLabel = 'label-primary';
			break;
	case 'Suspended':
			statusLabel = 'label-warning';
			break;
	case 'Closed':
			statusLabel = 'label-danger';
			break;
	}
	return statusLabel;
});

Template.registerHelper('financingOptionTerms', function () {
	var fot = [
		{
			label: "No financing option",
			value: "0"
		},{
			label: "30 days same as cash",
			value: "30"
		},{
			label: "60 days same as cash",
			value: "60"
		},{
			label: "90 days same as cash",
			value: "90"
		},{
			label: "120 days same as cash",
			value: "120"
		}
	];
	return fot;
});

Template.registerHelper('funnelProgressClass', function(currentProgress, progressRate) {
    var fp = "";

    if(currentProgress < progressRate){
        fp = "is-complete";
    }else if(currentProgress == progressRate){
        fp = "is-active";
    }

    return fp;
});

Template.registerHelper('formatDate', function(date) {
    return moment(date).calendar();
});

Template.registerHelper('selectAccountTypeOptions', function () {
		return [{
			label: "Residential",
			value: "Residential"
		}, {
			label: "Commercial",
			value: "Commercial"
		}, {
			label: "Industrial",
			value: "Industrial"
		}]
	});

Template.registerHelper('selectPropertyTypes', function() {
    return [{
        label: "Headquarters",
        value: "Headquarters"
    }, {
        label: "Corporate Office",
        value: "Corporate Office"
    }, {
        label: "Branch Office",
        value: "Branch Office"
    }, {
        label: "Wearhouse",
        value: "Wearhouse"
    }, {
        label: "Store Front",
        value: "Store Front"
    }, {
        label: "Work Shop",
        value: "Work Shop"
    }
  ]
});

Template.registerHelper('selectPropertyStatus', function() {
    return [{
        label: "Rent",
        value: "Rent"
    }, {
        label: "Lease",
        value: "Lease"
    }, {
        label: "Own",
        value: "Own"
    }]
});


Template.registerHelper('USStates', function() {
    return [{
        label: "Alabama",
        value: "AL"
    }, {
        label: "Alaska",
        value: "AK"
    }, {
        label: "American Samoa",
        value: "AS"
    }, {
        label: "Arizona",
        value: "AZ"
    }, {
        label: "Arkansas",
        value: "AR"
    }, {
        label: "British Columbia",
        value: "BC"
    }, {
        label: "California",
        value: "CA"
    }, {
        label: "Colorado",
        value: "CO"
    }, {
        label: "Connecticut",
        value: "CT"
    }, {
        label: "Delaware",
        value: "DE"
    }, {
        label: "District Of Columbia",
        value: "DC"
    }, {
        label: "Federated States Of Micronesia",
        value: "FM"
    }, {
        label: "Florida",
        value: "FL"
    }, {
        label: "Georgia",
        value: "GA"
    }, {
        label: "Guam",
        value: "GU"
    }, {
        label: "Hawaii",
        value: "HI"
    }, {
        label: "Idaho",
        value: "ID"
    }, {
        label: "Illinois",
        value: "IL"
    }, {
        label: "Indiana",
        value: "IN"
    }, {
        label: "Iowa",
        value: "IA"
    }, {
        label: "Kansas",
        value: "KS"
    }, {
        label: "Kentucky",
        value: "KY"
    }, {
        label: "Louisiana",
        value: "LA"
    }, {
        label: "Maine",
        value: "ME"
    }, {
        label: "Manitoba",
        value: "MB"
    }, {
        label: "Marshall Islands",
        value: "MH"
    }, {
        label: "Maryland",
        value: "MD"
    }, {
        label: "Massachusetts",
        value: "MA"
    }, {
        label: "Michigan",
        value: "MI"
    }, {
        label: "Minnesota",
        value: "MN"
    }, {
        label: "Mississippi",
        value: "MS"
    }, {
        label: "Missouri",
        value: "MO"
    }, {
        label: "Montana",
        value: "MT"
    }, {
        label: "Nebraska",
        value: "NE"
    }, {
        label: "Nevada",
        value: "NV"
    }, {
        label: "New Brunswick",
        value: "NB"
    }, {
        label: "New Hampshire",
        value: "NH"
    }, {
        label: "New Jersey",
        value: "NJ"
    }, {
        label: "New Mexico",
        value: "NM"
    }, {
        label: "New York",
        value: "NY"
    }, {
        label: "Newfoundland and Labrador",
        value: "NL"
    }, {
        label: "North Carolina",
        value: "NC"
    }, {
        label: "North Dakota",
        value: "ND"
    }, {
        label: "Northern Mariana Islands",
        value: "MP"
    }, {
        label: "Nova Scotia",
        value: "NS"
    }, {
        label: "Northwest Territories",
        value: "NT"
    }, {
        label: "Nunavut",
        value: "NU"
    }, {
        label: "Ohio",
        value: "OH"
    }, {
        label: "Oklahoma",
        value: "OK"
    }, {
        label: "Ontario",
        value: "ON"
    }, {
        label: "Oregon",
        value: "OR"
    }, {
        label: "Palau",
        value: "PW"
    }, {
        label: "Pennsylvania",
        value: "PA"
    }, {
        label: "Prince Edward Island",
        value: "PE"
    }, {
        label: "Puerto Rico",
        value: "PR"
    }, {
        label: "Quebec",
        value: "QC"
    }, {
        label: "Rhode Island",
        value: "RI"
    }, {
        label: "Saskatchewan",
        value: "SK"
    }, {
        label: "South Carolina",
        value: "SC"
    }, {
        label: "South Dakota",
        value: "SD"
    }, {
        label: "Tennessee",
        value: "TN"
    }, {
        label: "Texas",
        value: "TX"
    }, {
        label: "Utah",
        value: "UT"
    }, {
        label: "Vermont",
        value: "VT"
    }, {
        label: "Virgin Islands",
        value: "VI"
    }, {
        label: "Virginia",
        value: "VA"
    }, {
        label: "Washington",
        value: "WA"
    }, {
        label: "West Virginia",
        value: "WV"
    }, {
        label: "Wisconsin",
        value: "WI"
    }, {
        label: "Wyoming",
        value: "WY"
    }, {
        label: "Yukon",
        value: "YT"
    }]
});
